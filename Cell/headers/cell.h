class Cell
{
    private:
        int priority; // La priorité de la cellule : Permettra de définir si une cellule est une erreur
        int type; // Le type de la cellule : 1 pour des données fichier, 2 pour des données vidéo, 3 pour des données voix
    public:
        Cell(); // Constructeur vide
        Cell(int priority, int type);   // Constructeur avec les paramètres priority et type
        int getPriority();              // Accesseur pour la priorité
        int getType();                  // Accesseur pour le type
        void setPriority(int priority); // Mutateur de la priorité
        void setType(int type);         // Mutateur du type
        ~Cell();                        // Destructeur
};
