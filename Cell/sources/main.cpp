#include "cell.cpp"
#include <iostream>

using namespace std;

// Test du constructeur vide.
void test_void_constructor()
{
  Cell test_cell;
  cout << "Empty constructor done!" << endl;
}

// Test du constructeur avec les paramètres priority et type.
void test_constructor(int priority, int type)
{
  Cell test_cell(priority,type);
  cout << "Constructor with parameters done!" << endl;
}

// Test de la fonction getPriority.
void test_get_priority()
{
  Cell test_cell(0,3);
  cout << "On doit obtenir 0. On a test_cell.priority = " << test_cell.getPriority() << endl;
}

// Test de la fonction getType.
void test_get_type()
{
  Cell test_cell(0,3);
  cout << "On doit obtenir 3. On a test_cell.type = " << test_cell.getType() << endl;
}

// Test de la fonction setPriority. On créé une cellule avec le constructeur ave paramètres puis on modifie la priorité.
void test_set_priority(int priority)
{
  Cell test_cell(0,3);
  cout << "On doit obtenir 0 avant le set : test_cell.priority = " << test_cell.getPriority() << endl;
  test_cell.setPriority(priority);
  cout << "On doit obtenir la nouvelle priorite apres le set : test_cell.priority = " << test_cell.getPriority() << endl;
}

// Test de la fonction setType. On créé une cellule avec le constructeur ave paramètres puis on modifie le type.
void test_set_type(int type)
{
  Cell test_cell(0,3);
  cout << "On doit obtenir 3 avant le set : test_cell.type = " << test_cell.getType() << endl;
  test_cell.setType(type);
  cout << "On doit obtenir le nouveau type apres le set : test_cell.type = " << test_cell.getType() << endl;
}

// Main avec les tests
int main()
{
    test_void_constructor();
    test_constructor(0,3);
    test_get_priority();
    test_get_type();
    test_set_priority(1);
    test_set_type(2);
}
