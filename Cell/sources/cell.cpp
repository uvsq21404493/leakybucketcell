#include "../headers/Cell.h"
/**
* Constructeur vide de cell
* Entrées : aucune
* Sortie : aucune
* Auteur : Antoine
*/
Cell::Cell(){}

/**
* Constructeur vide de cell
* Entrées : int priority, int type
* Sortie : aucune
* Auteur : Antoine
*/
Cell::Cell(int priority, int type)
{
    this->priority = priority;
    this->type = type;
}

/**
* Destructeur de cell
* Entrées : aucune
* Sortie : aucune
* Auteur : Antoine
*/
Cell::~Cell(){}

/**
* Accesseur de priority
* Entrées : aucune
* Sortie : retourne priority
* Auteur : Antoine
*/
int Cell::getPriority()
{
    return this->priority;
}

/**
* Accesseur de type
* Entrées : aucune
* Sortie : retourne type
* Auteur : Antoine
*/
int Cell::getType()
{
    return this->type;
}

/**
* Mutateur de priority
* Entrées : int priority
* Sortie : aucune
* Auteur : Antoine
*/
void Cell::setPriority(int priority)
{
    this->priority = priority;
}

/**
* Mutateur de type
* Entrées : int priority
* Sortie : aucune
* Auteur : Antoine
*/
void Cell::setType(int type)
{
    this->type = type;
}
